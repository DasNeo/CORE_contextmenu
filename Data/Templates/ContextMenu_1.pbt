Assets {
  Id: 14126359262831853994
  Name: "ContextMenu"
  PlatformAssetType: 5
  TemplateAsset {
    ObjectBlock {
      RootId: 5494113610993253007
      Objects {
        Id: 5494113610993253007
        Name: "TemplateBundleDummy"
        Transform {
          Location {
          }
          Rotation {
          }
          Scale {
            X: 1
            Y: 1
            Z: 1
          }
        }
        Folder {
          BundleDummy {
            ReferencedAssets {
              Id: 14510135958703459320
            }
          }
        }
      }
    }
    PrimaryAssetId {
      AssetType: "None"
      AssetId: "None"
    }
  }
  Marketplace {
    Id: "7141f7d6f0cb49d388032f8bd9690365"
    OwnerAccountId: "bec4b82508f1496cb98161fda554e7d7"
    OwnerName: "DasNeo"
    Description: "With the default settings you can press Alt to enable the cursor and click on an object or player to show the menu.\r\n\r\nsee https://gitlab.com/DasNeo/CORE_contextmenu for more informations."
  }
  SerializationVersion: 97
}
