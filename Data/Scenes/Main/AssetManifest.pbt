Assets {
  Id: 16910278292812118833
  Name: "Sun Light"
  PlatformAssetType: 6
  PrimaryAsset {
    AssetType: "BlueprintAssetRef"
    AssetId: "CORESKY_SunLight"
  }
}
Assets {
  Id: 15704708847362345967
  Name: "Cylinder - Rounded"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_cylinder_rounded_002"
  }
}
Assets {
  Id: 12095835209017042614
  Name: "Cube"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_cube_002"
  }
}
Assets {
  Id: 11515840070784317904
  Name: "Skylight"
  PlatformAssetType: 6
  PrimaryAsset {
    AssetType: "BlueprintAssetRef"
    AssetId: "CORESKY_Skylight"
  }
}
Assets {
  Id: 7887238662729938253
  Name: "Sky Dome"
  PlatformAssetType: 6
  PrimaryAsset {
    AssetType: "BlueprintAssetRef"
    AssetId: "CORESKY_Sky"
  }
}
Assets {
  Id: 1255307216290173832
  Name: "Bush 01"
  PlatformAssetType: 1
  PrimaryAsset {
    AssetType: "StaticMeshAssetRef"
    AssetId: "sm_bush_generic_001"
  }
}
