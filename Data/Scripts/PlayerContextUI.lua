local contextFolder = World.GetRootObject():FindDescendantByName("PlayerContextUI"):FindDescendantByName("UI Panel")
local contextTitle = World.GetRootObject():FindDescendantByName("PlayerContextUI"):FindDescendantByName("ContextTitle")

local btn1 = contextFolder:FindDescendantsByName("UI Button")[1]
local btn2 = contextFolder:FindDescendantsByName("UI Button")[2]

function buttonClick(a)
    print("I got clicked!")
end

btn1.clickedEvent:Connect(buttonClick)

-- the function that is called to check if this script's click function should trigger
function Check(object)
    if(object == nil) then return false end
    if(object:IsA("Player")) then return true end
    return false
end

-- the function that is called when a players clicked on an object and our Check() passed
function Click(other)
    local cursorPosition = UI.GetCursorPosition()
    local screenSize = UI.GetScreenSize()
    if(screenSize.x < cursorPosition.x + contextFolder.width) then
        cursorPosition.x = screenSize.x - contextFolder.width
    end
    if(screenSize.y < cursorPosition.y + contextFolder.height) then
        cursorPosition.y = screenSize.y - contextFolder.height
    end
    contextFolder.y = cursorPosition.y
    contextFolder.x = cursorPosition.x
    contextFolder.visibility = Visibility.FORCE_ON
    contextTitle.text = other.name
end

-- the function that is called to hide the ui stuff
function Dismiss()
    contextFolder.visibility = Visibility.FORCE_OFF
end

local api = {}
api.Check = Check
api.Click = Click
api.Dismiss = Dismiss
api.test = "Player"
return api