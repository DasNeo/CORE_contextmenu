local settings = {
	AllowSelectionWhenSelected = true, 			-- Allows opening a context menu if another is open 
												-- when disabled, all BindingClick's will be ignored while a context menu is open
	DismissSelectionWhenReselecting = true, 	-- explicitly dismisses the context menu before opening the new
												-- does nothing if AllowSelectionWhenSelected is false
	DisableMovementWhenSelecting = true, 		-- disables the movement when cursor is shown
	BindingClick = "ability_primary",			-- Changes the button you need to press to open a context menu
	BindingDismiss = "ability_secondary",		-- (can be nil) Changes the button you need to press to close a context menu
	BindingShowCursor = "ability_extra_14",		-- Changes the button you need to press to show the cursor
}

local importedContextMenus = {}
local contextPrefix = "context_"

for name, value in pairs(script:GetCustomProperties()) do
    if string.sub(name,1,string.len(contextPrefix)) == contextPrefix then
        local test = require(value)
        table.insert(importedContextMenus, test)
    end
end

local openedContextMenu = nil

function OnBindingPressed(whichPlayer, binding)
	if (binding == settings["BindingShowCursor"]) then 
		local vis = UI.IsCursorVisible()
		UI.SetCursorVisible(not vis)
		whichPlayer.lookSensitivity = 0
		if(vis) then
			CloseMenu(whichPlayer)
		end
	elseif (binding == settings["BindingClick"] and UI.IsCursorVisible()) then
		local hitinfo = World.Raycast(whichPlayer:GetViewWorldPosition(), UI.GetCursorHitResult():GetImpactPosition()) or UI.GetCursorHitResult()
		if hitinfo == nil then 	
			print("hitinfo failed!") -- TODO: This should be fixed with the "or UI.GetCursorHitResult()"
			print(whichPlayer:GetViewWorldPosition())
			print(UI.GetCursorHitResult():GetImpactPosition())
			return
		end
		local target = hitinfo.other
		local targetType = type(target)
		for _,v in pairs(importedContextMenus) do
			if(v.Check(target)) then
				if(openedContextMenu ~= nil) then
					if(not settings["AllowSelectionWhenSelected"]) then return end
					if(settings["DismissSelectionWhenReselecting"]) then 
						openedContextMenu.Dismiss()
					end
				end
				v.Click(target)
				openedContextMenu = v
				return
			end
		end
	elseif (settings["BindingDismiss"] ~= nil and binding == settings["BindingDismiss"]) then
		CloseMenu(whichPlayer)
	end
end

function CloseMenu(whichPlayer)
	if(openedContextMenu ~= nil) then
		UI.SetCursorVisible(false)
		openedContextMenu.Dismiss()
		openedContextMenu = nil
	end
	whichPlayer.lookSensitivity = 1
end

function OnPlayerJoined(player)
	player.bindingPressedEvent:Connect(OnBindingPressed)
end

Game.playerJoinedEvent:Connect(OnPlayerJoined)