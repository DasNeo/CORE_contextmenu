This is a simple ContextMenu System for Core.

![Screenshot](https://image.prntscr.com/image/IcFaYkvuQ2er4cpdlPbFTQ.png)
![Screenshot](https://image.prntscr.com/image/qLTlicoBSnuFcx9fQ2apeQ.png)


Settings

BindingShowCursor = "ability_extra_14"
Sets the button you need to press to show the cursor (default alt key)

BindingClick = "ability_primary"
Sets the button you need to press to open a context menu (default left click)

BindingDismiss = "ability_secondary" (can be nil)
Sets the button you need to press to close a context menu (default right click)

AllowSelectionWhenSelected = true
Allows opening a context menu if another is open when disabled, all BindingClick's will be ignored while a context menu is open

DismissSelectionWhenReselecting = true
Explicitly dismisses the context menu before opening the new. Does nothing if AllowSelectionWhenSelected is false

DisableMovementWhenSelecting = true
Disables the movement when cursor is shown


To create a new ContextMenu you need to:

1. Create a UI for the ContextMenu. (You should wrap your UI inside a Panel so you can hide and move it easily)
2. Create a Script using the following template:
```lua
-- the function that is called to check if this script's click function should trigger
function Check(object)
end

-- the function that is called when a players clicked on an object and our Check() passed
function Click(other)
end

-- the function that is called to hide the ui stuff
function Dismiss()
end

local api = {}
api.Check = Check
api.Click = Click
api.Dismiss = Dismiss
return api
```
3. Add that script as a AssetReference to ContextMenuCore and name it context_*. (this is done so the core has all our contextmenus in one place. it reads all GetCustomProperties that start with "context_")
